# Contributor instructions

## To build the extension

First, install the necessary Node packages:

```shell
npm ci
```

Next, install the `vsce` program:

```shell
npm install --no-save vsce
```

Run the `vsce` script:

```shell
node_modules/vsce/vsce package
```

If all goes well, a new `.vsix` file will be generated at the top of the repository.
This file can be then installed manually into VS Code.

## To deploy a new version to OpenVSX

Increment the version number in `package.json`, and push your changes to `master`.

Once the pipeline passes, create a `vX.Y.Z` tag matching the version in the `package.json`: this will trigger a CI job that will automatically build and publish the new version.
