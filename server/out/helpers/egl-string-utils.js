"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EglSections = void 0;
const egl_static_section_parser_1 = require("./egl-static-section-parser");
const egl_ast_1 = require("./egl.ast");
const tokenizer_1 = require("./tokenizer");
class EglSections {
    constructor() {
        this._errors = [];
    }
    process(text, textDocument, connection, settings) {
        const lines = text.split("\n");
        const eglAst = new egl_ast_1.EglAst(settings);
        let lineNumber = 0;
        lines.forEach(line => {
            // Reject empty lines
            if (line.trim().length > 0) {
                this.matchStaticSectionContents(line).forEach(match => {
                    const tokenizer = new tokenizer_1.Tokenizer();
                    let tokens = tokenizer.process(match, lineNumber, line.indexOf(match));
                    this._errors = [
                        ...this._errors,
                        ...new egl_static_section_parser_1.EglStaticSectionParser(tokens, textDocument, connection, eglAst, settings).parse()
                    ];
                });
            }
            lineNumber++;
        });
        // Tokenize expressions
        lines.forEach(line => { });
        return this._errors;
    }
    matchStaticSectionContents(content) {
        const sectionContentsRegex = /\[%=(.*?)%/gm;
        let match;
        let matches = [];
        while (match = sectionContentsRegex.exec(content)) {
            matches.push(match[1]);
        }
        return matches;
    }
}
exports.EglSections = EglSections;
const eglSections = new EglSections();
const tokenize = new tokenizer_1.Tokenizer();
console.log(JSON.stringify(tokenize.process("mainPackage")));
// let result = tokenize.process(`
// [%=getBotWrittenWarning() %]
// package [%=mainPackage%].services;
// import [%=mainPackage%].entities.AbstractEntityAudit;
// import [%=mainPackage%].entities.[%=entityNamePascalCase%]Entity;
// import [%=mainPackage%].entities.[%=entityNamePascalCase%]EntityAudit;
// import [%=mainPackage%].entities.Q[%=entityNamePascalCase%]Entity;
// import [%=mainPackage%].repositories.[%=entityNamePascalCase%]Repository;
// import [%=mainPackage%].graphql.utils.Where;
// import [%=mainPackage%].graphql.utils.AuditQueryType;
// import [%=mainPackage%].services.utils.QuerydslUtils;
// import [%=mainPackage%].configs.security.auditing.CustomRevisionEntity;
// `);
// console.log(result);
/*
TODO: check for broken static sections
TODO: Check for broken dynamic sections
TODO: Check for broken comments

TODO: Handle addition operation in static section.
TODO: handle multiline static section.
TODO: Handle new lines in static content.
*/ 
//# sourceMappingURL=egl-string-utils.js.map