"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProtectedRegion = exports.EglAst = exports.Positioned = void 0;
class Positioned {
}
exports.Positioned = Positioned;
class EglAst {
    constructor(settings) {
        this._protectedRegions = [];
        this._settings = settings;
        this._prCount = 0;
    }
    get ProtectedRegions() {
        return this._protectedRegions;
    }
    addProtectedRegion(pr) {
        pr.Count = this._prCount++;
        this._protectedRegions.push(pr);
        let duplicates = this._protectedRegions.filter(_pr => _pr.Id == pr.Id);
        return duplicates;
    }
}
exports.EglAst = EglAst;
class ProtectedRegion extends Positioned {
    constructor(id, startPosition, endPosition) {
        super();
        this._id = id;
        this.startPosition = startPosition;
        this.endPosition = endPosition;
    }
    set Flagged(flag) {
        this._flagged = flag;
    }
    get Flagged() {
        return this._flagged;
    }
    getUniqueId() {
        return `${this.startPosition.line}-${this.startPosition.character}--${this.Id}`;
    }
    set Count(count) {
        this._count = count;
    }
    get Count() {
        return this._count;
    }
    get Id() {
        return this._id;
    }
}
exports.ProtectedRegion = ProtectedRegion;
//# sourceMappingURL=egl.ast.js.map