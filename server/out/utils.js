"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UriUtils = void 0;
const languages_enum_1 = require("./interfaces/languages.enum");
class UriUtils {
    static getLanguage(uri) {
        if (uri == null) {
            return null;
        }
        const extension = uri.substring(uri.length - 3, uri.length);
        let language = languages_enum_1.Language.EOL;
        switch (extension) {
            case 'egl':
                language = languages_enum_1.Language.EGL;
                break;
            case 'egx':
                language = languages_enum_1.Language.EGX;
                break;
            case 'eol':
                language = languages_enum_1.Language.EOL;
                break;
        }
        return language;
    }
}
exports.UriUtils = UriUtils;
//# sourceMappingURL=utils.js.map