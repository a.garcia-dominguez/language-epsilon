"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Language = void 0;
var Language;
(function (Language) {
    Language["EOL"] = "language-eol";
    Language["EGX"] = "language-egx";
    Language["EGL"] = "language-egl";
})(Language = exports.Language || (exports.Language = {}));
//# sourceMappingURL=languages.enum.js.map