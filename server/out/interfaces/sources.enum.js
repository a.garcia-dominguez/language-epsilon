"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Source = void 0;
var Source;
(function (Source) {
    Source["epsilonLanguage"] = "Epsilon Language";
    Source["epsilonLanguageEgl"] = "Epsilon Language EGL";
    Source["epsilonLanguageEol"] = "Epsilon Language EOL";
    Source["epsilonLanguageEgx"] = "Epsilon Language EGX";
})(Source = exports.Source || (exports.Source = {}));
//# sourceMappingURL=sources.enum.js.map