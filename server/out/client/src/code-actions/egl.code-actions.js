"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vscode_1 = require("vscode");
const COMMAND = "code-actions-quickfix.fix-white-space";
var StaticSectionActionCode;
(function (StaticSectionActionCode) {
    StaticSectionActionCode["StartSpace"] = "static-section-start-space";
    StaticSectionActionCode["EndSpace"] = "static-section-end-space";
})(StaticSectionActionCode = exports.StaticSectionActionCode || (exports.StaticSectionActionCode = {}));
var DynamicSectionActionCode;
(function (DynamicSectionActionCode) {
    DynamicSectionActionCode["StartSpace"] = "dynamic-section-start-space";
    DynamicSectionActionCode["EndSpace"] = "dynamic-section-end-space";
})(DynamicSectionActionCode = exports.DynamicSectionActionCode || (exports.DynamicSectionActionCode = {}));
class FixWhiteSpace {
    provideCodeActions(document, range, context, token) {
        return context.diagnostics
            .filter(diagnostic => StaticSectionActionCode[diagnostic.code])
            .map(diagnostic => this.createCommandCodeAction(document, range, diagnostic));
    }
    createCommandCodeAction(document, range, diagnostic) {
        if (StaticSectionActionCode[diagnostic.code] === StaticSectionActionCode.EndSpace) {
            return this.fixEndSpace(document, range, diagnostic);
        }
        else if (StaticSectionActionCode[diagnostic.code] === StaticSectionActionCode.StartSpace) {
            return this.fixStartSpace(document, range, diagnostic);
        }
    }
    fixEndSpace(document, range, diagnostic) {
        const action = new vscode_1.CodeAction("Add missing end space", vscode_1.CodeActionKind.QuickFix);
        action.command = {
            command: COMMAND,
            title: "Auto fix white space at the end of the static section",
            tooltip: "This will attempt to auto fix the white space at the end of the static section."
        };
        action.edit = new vscode_1.WorkspaceEdit();
        const sectionRange = document.getWordRangeAtPosition(new vscode_1.Position(range.start.line, range.start.character), /%]/);
        const content = document.getText(sectionRange);
        // Perform fix
        const fixedContent = content.replace("%]", " %]");
        action.edit.replace(document.uri, sectionRange, fixedContent);
        action.diagnostics = [diagnostic];
        return action;
    }
    fixStartSpace(document, range, diagnostic) {
        const action = new vscode_1.CodeAction("Add missing start space.", vscode_1.CodeActionKind.QuickFix);
        action.command = {
            command: COMMAND,
            title: "Auto fix white space at the start of the static section",
            tooltip: "This will attempt to auto fix the white space at the start of the static section."
        };
        action.edit = new vscode_1.WorkspaceEdit();
        const sectionRange = document.getWordRangeAtPosition(new vscode_1.Position(range.start.line, range.start.character), /\[%=(.*?)%]/);
        const content = document.getText(sectionRange);
        // Perform fix
        const fixedContent = content.replace("[%=", "[%= ");
        action.edit.replace(document.uri, sectionRange, fixedContent);
        action.diagnostics = [diagnostic];
        return action;
    }
}
FixWhiteSpace.providedCodeActionKinds = [
    vscode_1.CodeActionKind.QuickFix
];
exports.FixWhiteSpace = FixWhiteSpace;
//# sourceMappingURL=egl.code-actions.js.map