"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Positioned {
}
exports.Positioned = Positioned;
class EglAst {
    constructor(settings) {
        this._protectedRegions = [];
        this._settings = settings;
    }
    get ProtectedRegions() {
        return this._protectedRegions;
    }
    addProtectedRegion(pr) {
        let duplicates = this._protectedRegions.filter(_pr => _pr.Id == pr.Id);
        this._protectedRegions.push(pr);
        return duplicates;
    }
}
exports.EglAst = EglAst;
class ProtectedRegion extends Positioned {
    get Id() {
        return this._id;
    }
    constructor(id, startPosition, endPosition) {
        super();
        this._id = id;
        this.startPosition = startPosition;
        this.endPosition = endPosition;
    }
}
exports.ProtectedRegion = ProtectedRegion;
//# sourceMappingURL=egl.ast.js.map