"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TokenKeys;
(function (TokenKeys) {
    TokenKeys["SPACE"] = "SPACE";
    TokenKeys["L_BRACKET"] = "L_BRACKET";
    TokenKeys["R_BRACKET"] = "R_BRACKET";
    TokenKeys["IDENTIFIER"] = "IDENTIFIER";
    TokenKeys["OP_PLUS"] = "OP_PLUS";
    TokenKeys["NEW_LINE"] = "NEW_LINE";
    TokenKeys["EOF"] = "EOF";
    TokenKeys["KW_PROTECTED"] = "KW_PROTECTED";
    TokenKeys["KW_OUT"] = "KW_OUT";
    TokenKeys["SINGLE_QUOTE"] = "SINGLE_QUOTE";
    TokenKeys["DOUBLE_QUOTE"] = "DOUBLE_QUOTE";
    TokenKeys["COMMA"] = "COMMA";
    TokenKeys["DOT"] = "DOT";
})(TokenKeys = exports.TokenKeys || (exports.TokenKeys = {}));
exports.TOKEN_MATCHERS = {
    [TokenKeys.SPACE]: /\s/gm,
    [TokenKeys.L_BRACKET]: /\(/,
    [TokenKeys.R_BRACKET]: /\)/,
    [TokenKeys.SINGLE_QUOTE]: /\'/,
    [TokenKeys.DOUBLE_QUOTE]: /"/,
    [TokenKeys.DOT]: /\./,
    [TokenKeys.KW_PROTECTED]: /protected/,
    [TokenKeys.KW_OUT]: /out/,
    [TokenKeys.IDENTIFIER]: /\w+/,
    [TokenKeys.OP_PLUS]: /\+/,
    [TokenKeys.NEW_LINE]: /\n/,
    [TokenKeys.COMMA]: /,/,
};
exports.EOF_TOKEN = {
    name: "EOF",
    value: "EOF",
    location: {
        start: 0,
        end: 0,
        line: 0
    }
};
//# sourceMappingURL=tokens.js.map