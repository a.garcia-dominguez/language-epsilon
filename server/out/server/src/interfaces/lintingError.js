"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LogLevel;
(function (LogLevel) {
    LogLevel["DEBUG"] = "debug";
    LogLevel["INFO"] = "info";
    LogLevel["ERROR"] = "error";
    LogLevel["NONE"] = "none";
})(LogLevel = exports.LogLevel || (exports.LogLevel = {}));
//# sourceMappingURL=lintingError.js.map