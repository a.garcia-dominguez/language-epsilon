"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ResolveEgxCompletion = exports.EgxCompletions = void 0;
const vscode_languageserver_types_1 = require("vscode-languageserver-types");
exports.EgxCompletions = [
    {
        label: 'rule ',
        kind: vscode_languageserver_types_1.CompletionItemKind.Keyword,
        data: 1
    },
    {
        label: 'transform ',
        kind: vscode_languageserver_types_1.CompletionItemKind.Keyword,
        data: 2
    },
    {
        label: 'guard ',
        kind: vscode_languageserver_types_1.CompletionItemKind.Keyword,
        data: 3
    },
    {
        label: 'transform ',
        kind: vscode_languageserver_types_1.CompletionItemKind.Keyword,
        data: 4
    }
];
function ResolveEgxCompletion(item) {
    if (item.data === 1) {
        item.detail = 'rule <name>';
        item.documentation =
            `Allows transformation of an arbitrary number of source models to the target model.
rules can also be extended by other rules (comma separated) using the extends keyword.`;
    }
    else if (item.data === 2) {
        item.detail = 'transform <sourceParameterName>:<sourceParameterType>';
        item.documentation = '';
    }
    else if (item.data === 3) {
        item.detail = 'guard (:expression)|({statementBlock}))?';
        item.documentation =
            `Use an expression that returns a boolean for your guard
e.g  guard: userEntityMap().containsKey(entity.Name).A guard is part of a rule.`;
    }
    return item;
}
exports.ResolveEgxCompletion = ResolveEgxCompletion;
//# sourceMappingURL=egx.completion.js.map