import { CompletionItem, CompletionItemKind } from 'vscode-languageserver-types';
export const EglCompletions = [
	{
		label: 'cached',
		kind: CompletionItemKind.Property,
		data: 1,
		insertText: '@cached\n',
		detail: 'Operation Result Caching',
		documentation: 'Caching of the result of the operation, simply add this annotation to any operation for'
			+ 'the result to be cached'
	},
	{
		label: 'operation',
		kind: CompletionItemKind.Keyword,
		data: 2,
		detail: "Operation",
		documentation: 'Definition of the kind of objects in with this operation is applicable (context),'
			+ 'a name, parameters and optionally a return type.'

	},
];

export function ResolveEglCompletion(item: CompletionItem) {
	return item;
}