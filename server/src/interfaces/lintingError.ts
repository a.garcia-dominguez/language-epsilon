import { DiagnosticSeverity } from 'vscode-languageserver';

export interface LintingError {
	match: RegExp;
	message: string;
	severity: DiagnosticSeverity;
}

export interface LintSettings {
	lintStyle: boolean;
	lintDuplicateProtectedRegionIds: boolean;
	maxNumberOfProblems: number;
	logLevel: LogLevel;
}

export enum LogLevel {
	DEBUG = 'debug',
	INFO = 'info',
	ERROR = 'error',
	NONE = 'none'
}