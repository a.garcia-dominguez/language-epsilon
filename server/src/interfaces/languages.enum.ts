export enum Language {
	EOL = 'language-eol',
	EGX = 'language-egx',
	EGL = 'language-egl'
}