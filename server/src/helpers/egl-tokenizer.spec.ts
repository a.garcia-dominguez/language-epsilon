import { expect } from "chai";
import "mocha";
import { EOF_TOKEN, Token } from '../interfaces/tokens';
import { EglSections } from './egl-string-utils';
import { TokenStream } from './token-stream';
import { Tokenizer } from './tokenizer';

describe("Match static section", () => {
	const tokenizer = new EglSections();

	it("Match single identifier in static section", () => {
		const line = "import [%=mainPackage%].entities.AbstractEntityAudit;";

		const result = tokenizer.matchStaticSectionContents(line)[0];
		const expectedResult = "mainPackage";

		expect(result).to.equal(expectedResult);
	});

	it("Match multiple static sections in the same line", () => {
		const line = "import [%=mainPackage%].entities.[%= test() %];";

		const result = tokenizer.matchStaticSectionContents(line);
		const expectedResult = ["mainPackage", " test() "];

		expect(result[0]).to.equal(expectedResult[0]);
		expect(result[1]).to.equal(expectedResult[1]);
	});
});

describe("Process static section matches", () => {
	const tokenizer = new Tokenizer();

	it("Process single identifier in static section", () => {
		const match = "mainPackage";
		const lineNumber = 4;
		const start = 4;

		const result = tokenizer.process(
			match,
			lineNumber,
			start
		);

		expect(result.currentToken().name).to.equal('IDENTIFIER');
		expect(result.currentToken().value).to.equal(match);
		expect(result.currentToken().location.start).to.equal(start);
		expect(result.currentToken().location.end).to.equal(start + 11);
		expect(result.currentToken().location.line).to.equal(lineNumber);
	});

	describe('Token stream', () => {
		const tokens = [
			{
				name: "SPACE",
				value: " ",
				location: {
					start: 0,
					end: 1,
					line: 1
				}
			},
			{
				name: "L_BRACKET",
				value: "(",
				location: {
					start: 1,
					end: 2,
					line: 1
				}
			}
		];

		it('Return current token', () => {
			const tokenStream = new TokenStream(tokens);
			const result = tokenStream.currentToken();

			expect(result).to.equal(tokens[0]);
		});

		it('Get next token', () => {
			const tokenStream = new TokenStream(tokens);
			const result = tokenStream.nextToken();

			expect(result).to.equal(tokens[1]);
		});

		it("Consume token", () => {
			const tokenStream = new TokenStream(tokens);
			const result = tokenStream.consumeToken();

			expect(result).to.equal(tokens[0]);
		});

		it("Consume token and check current", () => {
			const tokenStream = new TokenStream(tokens);
			tokenStream.consumeToken();

			const result = tokenStream.currentToken();

			expect(result).to.equal(tokens[1]);
		});

		it("Check previous for first token", () => {
			const tokenStream = new TokenStream(tokens);
			const result = tokenStream.previousToken();


			expect(result).to.equal(EOF_TOKEN);
		});

		it("Check next for last token", () => {
			const tokenStream = new TokenStream(tokens);
			tokenStream.consumeToken();
			const result = tokenStream.nextToken();

			expect(result).to.equal(EOF_TOKEN);
		});

		// it("checkNextSet for two tokens", () => {
		// 	const tokenStream = new TokenStream(tokens);
		// 	tokenStream.consumeToken();
		// 	const result = tokenStream.nextToken();

		// 	expect(result).to.equal(SpecialTokens.EOF);
		// });
	});

	describe('EglStaticSectionParser', () => {
		const tokens: Array<Token> = [{
			name: 'IDENTIFIER',
			value: 'mainPackage',
			location: { start: 1, end: 12, line: 1 }
		},
		{
			name: 'L_BRACKET',
			value: '(',
			location: { start: 12, end: 13, line: 1 }
		},
		{
			name: 'R_BRACKET',
			value: ')',
			location: { start: 13, end: 14, line: 1 }
		}];

		const tokenStream = new TokenStream(tokens);

	});
});
