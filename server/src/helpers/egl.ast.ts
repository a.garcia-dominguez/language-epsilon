import { Position } from 'vscode-languageserver';
import { LintSettings } from '../interfaces/lintingError';


export abstract class Positioned {
	startPosition: Position;
	endPosition: Position;
}


export class EglAst {
	private _protectedRegions: Array<ProtectedRegion> = [];
	private _settings: LintSettings;
	private _prCount: number;

	constructor(settings: LintSettings) {
		this._settings = settings;
		this._prCount = 0;
	}

	get ProtectedRegions() {
		return this._protectedRegions;
	}

	addProtectedRegion(pr: ProtectedRegion): Array<ProtectedRegion> {
		pr.Count = this._prCount++;
		this._protectedRegions.push(pr);

		let duplicates = this._protectedRegions.filter(_pr => _pr.Id == pr.Id);

		return duplicates;
	}
}

export class ProtectedRegion extends Positioned {
	private _id: string;
	private _count: number;
	private _flagged: boolean;

	set Flagged(flag: boolean) {
		this._flagged = flag;
	}

	get Flagged() {
		return this._flagged
	}

	getUniqueId() {
		return `${this.startPosition.line}-${this.startPosition.character}--${this.Id}`;
	}

	set Count(count: number) {
		this._count = count;
	}

	get Count() {
		return this._count;
	}

	get Id() {
		return this._id;
	}

	constructor(id: string, startPosition: Position, endPosition: Position) {
		super();
		this._id = id;
		this.startPosition = startPosition;
		this.endPosition = endPosition;
	}
}

