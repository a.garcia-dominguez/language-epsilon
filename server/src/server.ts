/* --------------------------------------------------------------------------------------------
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 * ------------------------------------------------------------------------------------------ */

import { CompletionItem, createConnection, DidChangeConfigurationNotification, InitializeParams, ProposedFeatures, TextDocumentPositionParams, TextDocuments } from 'vscode-languageserver';
import { EgxCompletions, ResolveEgxCompletion } from './completions/egx.completion';
import { EglCompletions, ResolveEglCompletion } from './completions/elg.completions';
import { EolCompletions, ResolveEolCompletion } from './completions/eol.completions';
import { validateEGLDocument } from './egl';
import { validateEGXDocument } from './egx';
import { validateEOLDocument } from './eol';
import { Language } from './interfaces/languages.enum';
import { LogLevel } from './interfaces/lintingError';
import { UriUtils } from './utils';

// Create a connection for the server. The connection uses Node's IPC as a transport.
// Also include all preview / proposed LSP features.
let connection = createConnection(ProposedFeatures.all);

// Create a simple text document manager. The text document manager
// supports full document sync only
let documents: TextDocuments = new TextDocuments();

let hasConfigurationCapability: boolean = false;
let hasWorkspaceFolderCapability: boolean = false;
let hasDiagnosticRelatedInformationCapability: boolean = false;

connection.onInitialize((params: InitializeParams) => {
	let capabilities = params.capabilities;

	// Does the client support the `workspace/configuration` request?
	// If not, we will fall back using global settings
	hasConfigurationCapability = !!(capabilities.workspace && !!capabilities.workspace.configuration);
	hasWorkspaceFolderCapability = !!(capabilities.workspace && !!capabilities.workspace.workspaceFolders);
	hasDiagnosticRelatedInformationCapability =
		!!(capabilities.textDocument &&
			capabilities.textDocument.publishDiagnostics &&
			capabilities.textDocument.publishDiagnostics.relatedInformation);

	return {
		capabilities: {
			textDocumentSync: documents.syncKind,
			// Tell the client that the server supports code completion
			completionProvider: {
				resolveProvider: true
			}
		}
	};
});

connection.onInitialized(() => {
	if (hasConfigurationCapability) {
		// Register for all configuration changes.
		connection.client.register(
			DidChangeConfigurationNotification.type,
			undefined
		);
	}
	if (hasWorkspaceFolderCapability) {
		connection.workspace.onDidChangeWorkspaceFolders(_event => {
			connection.console.log('Workspace folder change event received.');
		});
	}
});


interface Settings {
	maxNumberOfProblems: number;
	lintStyle: boolean;
	lintDuplicateProtectedRegionIds: boolean;
	logLevel: LogLevel;
}

// The global settings, used when the `workspace/configuration` request is not supported by the client.
// Please note that this is not the case when using this server with the client provided in this example
// but could happen with other clients.
const defaultSettings: Settings = {
	maxNumberOfProblems: 1000,
	lintStyle: true,
	lintDuplicateProtectedRegionIds: true,
	logLevel: LogLevel.NONE
};
let globalSettings: Settings = defaultSettings;

// Cache the settings of all open documents
let documentSettings: Map<string, Thenable<Settings>> = new Map();

connection.onDidChangeConfiguration(change => {
	if (hasConfigurationCapability) {
		// Reset all cached document settings
		documentSettings.clear();
	} else {
		globalSettings = <Settings> (
			(change.settings.epsilonLanguagesSettings || defaultSettings)
		);
	}

	// Revalidate all open text documents
	documents.all().forEach((document) => {
		switch (document.languageId) {
			case 'language-eol':
				validateEOLDocument(document, connection, hasDiagnosticRelatedInformationCapability);
				break;
			case 'language-egl':
				validateEGLDocument(document, connection, hasDiagnosticRelatedInformationCapability);
				break;
			case 'language-egx':
				validateEGXDocument(document, connection, hasDiagnosticRelatedInformationCapability);
				break;
			default:
				return;

		}
	});
});

export function getDocumentSettings(resource: string): Thenable<Settings> {
	if (!hasConfigurationCapability) {
		return Promise.resolve(globalSettings);
	}
	let result = documentSettings.get(resource);
	if (!result) {
		result = connection.workspace.getConfiguration({
			scopeUri: resource,
			section: 'languageServerEpsilon'
		});
		documentSettings.set(resource, result);
	}
	return result;
}

// Only keep settings for open documents
documents.onDidClose(e => {
	documentSettings.delete(e.document.uri);
});

// The content of a text document has changed. This event is emitted
// when the text document first opened or when its content has changed.
documents.onDidChangeContent(change => {
	connection.console.log(`Change detected for language ID -> ${change.document.languageId}`);
	triggerParse(change);
});

documents.onDidSave(save => {
	connection.console.log(`Save detected for language ID -> ${save.document.languageId}`);
	// triggerParse(save);
})

connection.onDidChangeWatchedFiles(_change => {
	// Monitored files have change in VSCode
	connection.console.log('We received a file change event');
});

function triggerParse(change) {
	switch (change.document.languageId) {
		case Language.EOL:
			validateEOLDocument(change.document, connection, hasDiagnosticRelatedInformationCapability);
			break;
		case Language.EGL:
			validateEGLDocument(change.document, connection, hasDiagnosticRelatedInformationCapability);
			break;
		case Language.EGX:
			validateEGXDocument(change.document, connection, hasDiagnosticRelatedInformationCapability);
			break;
		default:
			return;

	}
}


// -----------------------------------------------------------------------------
//  ██████╗ ██████╗ ███╗   ███╗██████╗ ██╗     ███████╗████████╗██╗ ██████╗ ███╗   ██╗███████╗
// ██╔════╝██╔═══██╗████╗ ████║██╔══██╗██║     ██╔════╝╚══██╔══╝██║██╔═══██╗████╗  ██║██╔════╝
// ██║     ██║   ██║██╔████╔██║██████╔╝██║     █████╗     ██║   ██║██║   ██║██╔██╗ ██║███████╗
// ██║     ██║   ██║██║╚██╔╝██║██╔═══╝ ██║     ██╔══╝     ██║   ██║██║   ██║██║╚██╗██║╚════██║
// ╚██████╗╚██████╔╝██║ ╚═╝ ██║██║     ███████╗███████╗   ██║   ██║╚██████╔╝██║ ╚████║███████║
//  ╚═════╝ ╚═════╝ ╚═╝     ╚═╝╚═╝     ╚══════╝╚══════╝   ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝╚══════╝
//
// -----------------------------------------------------------------------------
const CompletionsMap = {
	[Language.EOL]: EolCompletions,
	[Language.EGX]: EgxCompletions,
	[Language.EGL]: EglCompletions
};
let currentLanguage: Language;

connection.onCompletion(
	(_textDocumentPosition: TextDocumentPositionParams): CompletionItem[] => {
		const uri = _textDocumentPosition.textDocument.uri;
		currentLanguage = UriUtils.getLanguage(uri);
		return CompletionsMap[currentLanguage];
	}
);


// This handler resolves additional information for the item selected in
// the completion list.

const ResolutionMap = {
	[Language.EOL]: ResolveEolCompletion,
	[Language.EGX]: ResolveEgxCompletion,
	[Language.EGL]: ResolveEglCompletion
};

connection.onCompletionResolve(
	(item: CompletionItem): CompletionItem => {
		return ResolutionMap[currentLanguage](item);
	}
);

// Make the text document manager listen on the connection
// for open, change and close text document events
documents.listen(connection);

// Listen on the connection
connection.listen();
