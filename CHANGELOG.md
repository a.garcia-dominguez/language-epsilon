# Change Log
## 1.0.0
- Syntax highlighting for EOL, EGX and EGL
- Snippets for EOL, EGX and EGL
- Boiler plate for language server

## 1.0.1
- Basic opinionated linting

## 1.0.2
- Fix indenting issue (auto indent was incorrect)
- Update snippets to match style linting

## 1.0.3
- Minor updates to linting rules (EGL template expression tags now require spaces before and after the expression)
- Add autocomplete for eol type methods.

## 1.0.4
- Minor spacing update for autocomplete
- Update snippets for egx

## 1.0.5
- Linting errors for duplicate protected regions

## 1.1.0
- Use tokenizer and basic parser for EGL static contexts
- Move some autocomplete options to client for better control
- Expand linting to provide more detail to duplicate protected regions
- Start boiler plate for code provider implementation for static section whitespace quick fix.

## 1.2.0
- Add basic formatter to fix whitespace linting errors between start and end of static regions
- Add quick fix code actions for whitespace start and end of protected region warnings.
- All duplicate protected regions are now flagged, even the original

## 1.2.1
- Add ETL syntax highlighting